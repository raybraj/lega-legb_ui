import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { type } from 'os';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  downloadFile(reporttype, groupid, fromdate, todate): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
    };
    let body = new URLSearchParams();
    body.set('groupid', groupid);
    body.set('fromdate', fromdate);
    body.set('todate', todate);
    alert(reporttype);
    if (reporttype == "legA") {
      return this.http.post('http://localhost:3000/legamin', String(body), httpOptions);
    }
    else {
      return this.http.post<any>('http://localhost:3000/legbmin', String(body), httpOptions);
    }

  }
}
