import { Component } from '@angular/core';
import { ApiService } from './service/api.service';
import * as FileSaver from "file-saver";
import * as moment from 'moment';
import { formatDate } from '@angular/common'
import { HttpResponse } from '@angular/common/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'biz';

  constructor(private api: ApiService) {

  }
  cloudReport: any = "legB";
  cloudReport_array: any = ["legA", "legB"];
  groupid: any = "";
  groupid_array: any = ['32146', '32141', '321463', '32149'];
  selected: { start: '2020-09-03T23:59', end: '2020-09-5T23:59' };
  sdate: moment.Moment;
  tdate: any;
  fromdate: any;
  todate: any;
  datesUpdated(e) {
    console.log(e);
    if (e != null) {
      this.legADownload
      const format = 'yyyy-MM-ddThh:mm';
      const locale = 'en-US';
      this.fromdate = formatDate(e.startDate._d, format, locale);
      this.todate = formatDate(e.endDate._d, format, locale);
      // console.log(this.fromdate);
      // console.log('----' + this.todate);
    }

  }

  onChangeGid(e) {
    if (e != null) {
      this.legADownload
      this.groupid = e;
    }
  }
  onChangeR(e) {
    if (e != null) {
      this.legADownload;
      this.cloudReport = e;
    }
  }

  legADownload() {
    // let filename = "../assets/upload/genCloudMinutesSheet/myfile.xlsx";
    return this.api.downloadFile(this.cloudReport, this.groupid, this.fromdate, this.todate)
      .subscribe((res) => {
        // const blob = new Blob([res], { type: 'text/xlsx' });
        FileSaver.saveAs(res)
      }
      );
  }

}
//